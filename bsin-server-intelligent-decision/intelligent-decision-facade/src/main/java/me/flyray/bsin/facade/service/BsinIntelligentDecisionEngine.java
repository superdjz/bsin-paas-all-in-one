package me.flyray.bsin.facade.service;

import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("intelligentDecisionEngine")
public interface BsinIntelligentDecisionEngine {

    /**
     * 智能决策引擎入口
     * @param requestMap
     * @return
     * @throws ClassNotFoundException
     */
    @POST
    @Path("execute")
    @Produces("application/json")
    public Map<String, Object> execute(Map<String, Object> requestMap);

}
