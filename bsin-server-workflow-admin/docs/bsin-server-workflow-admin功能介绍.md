## bsin-server-workflow-admin工作流后台管理系统

#### 一、介绍

​		bsin-server-workflow-admin是一款有关工作流流程设计的后台管理系统，它的主要功能是对流程、表单进行设计和部署以及对运行中的流程进行管理控制。 它将工作流的定义流程业务化，能够通过可视的、易理解的、规范化的操作方式来让业务人员定义工作流。 

#### 二、技术架构

​	 ++ sofaboot

 	++ mybatis

​	 ++ flowable

​	 ++ zookeeper

​     ++ seata

#### 三、业务流程

![avatar](../docs/image/微信图片_20221116163920.png)


​		这是工作流的大致流程，bsin-server-workflow-admin 主要解决的是流程的设计和部署，以及对流程实例和流程任务的管理控制。

#### 四、业务模块

##### 		1、表单设计

​			 通过页面可视化的方式来创建表单，使用鼠标拖拽相应组件即可完成。 

​			![avatar](image/表单设计.png)

​		左边是表单组件，系统支持输入框、多行输入框、日期选择、数字输入框、开关、单选、多选等基础组件自由组合的形成个性化表单，右边是组件的相关配置和表单样式的配置

​		在表单设计完成之后，表单会以标准的json格式保存到数据库中，示例：

```json
 "formDefinition": {
    "name": "请假申请表A",
    "key": "A",
    "fields": [
      {
        "fieldType": "FormField",
        "id": "name",
        "name": "姓名",
        "type": "text",
        "value": null,
        "required": false,
        "readOnly": false,
        "overrideId": false,
        "placeholder": null,
        "layout": null,
        "params": {}
      },
      {
        "fieldType": "FormField",
        "id": "days",
        "name": "请假天数",
        "type": "text",
        "value": null,
        "required": false,
        "readOnly": false,
        "overrideId": true,
        "placeholder": null,
        "layout": null,
        "params": {}
      },
        {
        "fieldType": "FormField",
        "id": "reason",
        "name": "请假事由",
        "type": "text",
        "value": null,
        "required": false,
        "readOnly": false,
        "overrideId": true,
        "placeholder": null,
        "layout": null,
        "params": {}
      }
    ],
    "outcomes": []
  }
```

​	*注意：表单样式都可以通过params进行保存*

​	BsinAdminModelService服务主要提供对流程模型、表单模型的增删改查以及流程和表单的部署。表单保存之后，我们在流程设计的时候就可以选择我们需要的表单，并通过formKey与流程模型进行绑定

##### 		2、表单管理

​			提供对表单的增删改查

![avatar](../docs/image/表单管理.png)
###### 		

##### 		3、模型设计器

通过鼠标拖拉拽的形式设计自己需要的流程，下图以请假流程示例：

![avatar](../docs/image/流程设计.png)
###### 1、请假申请流程设计：

（1）、填写请假申请，并发起请假流程

（2）、互斥网关会根据申请条件判断是经理审批还是系统审批

（3）、如果请假天数大于三天，审批任务会下发到经理的待办任务中；如果请假天数小于等于三天，则进				行系统审批（服务任务会调用后端的方法逻辑进行处理）

（4）、如果经理审批通过或者系统审批通过，则请假申请通过

###### 2、设计过程

**（1）、在开始节点，选择我们提前设计好的请假表单进行绑定**

​	![avatar](../docs/image/绑定表单.png)
**（2）、设置线段的筛选条件**

<img src="C:\Users\86188\AppData\Roaming\Typora\typora-user-images\1673510765769.png" alt="1673510765769" style="zoom:50%;" />**（3）、设置用户任务配置信息**

![avatar](../docs/image/设置线段的筛选条件.png)
​	用户任务的基础设置：

- 执行监听器

  执行监听器则监听流程的所有节点和连线。主要有start、end、take事件。其中节点有start、end两种事件，而连线则有take事件 

- 任务监听器

   任务监听器顾名思义是监听任务的。任务监听器的生命周期会经历assignment、create、complete、delete。当[流程引擎](https://so.csdn.net/so/search?q=流程引擎&spm=1001.2101.3001.7020)触发这四种事件类型时，对应的任务监听器会捕获其事件类型，再按照监听器的处理逻辑进行处理 

- 审批人设置

  ![avatar](../docs/image/审批人设置.png)
  审批人类型有四种

  ​	一、指定发布者为审批人

  ​	二、指定审批人（指定组织机构中的用户为审批人）

  ​	三、指定候选人（候选人支持多人）

  ​	四、固定值（可以直接填写用户名，同时也支持表达式）

  除此之外，它还支持多人审批，通过设置会签的相关参数，在流程运转的过程中实现多人审批

  ![avatar](../docs/image/会签.png)

会签类型分为串行、并行两种， 并行会签的意思是 多个人同时执行任务。顺序会签是按顺序执行任务。 

集合名—— 会签人员的集合变量，可以随意命名。 

元素名——循环集合多实例集合的变量名，可以随意命名。注意没有`${}`

```java
// 类似于这样的
for (String assignee : userList) {
  
}
```

审批人—— 分配用户的变量名称和元素变量的名称需要保持一致 

通过条件——设置任务完成的条件

默认流程变量：

nrOfInstances：会签中总共的实例数量

nrOfCompletedInstances：已经完成的实例数量

nrOfActiviteInstances：当前活动的实例数量，即还未完成的实例数量

所以该项可设置，例如：

${nrOfInstances == nrOfCompletedInstances}：表示所有人都需要完成任务会签才结束

${nrOfCompletedInstances == 1}：表示一个人完成任务，会签结束

其他条件可以以此类推实现

- 表单绑定

  同开始节点的表单绑定相同



**（4）设置服务任务的配置信息**

![avatar](../docs/image/服务任务.png)

服务任务一般由系统自动完成，当流程走到这一步的时候，不会自动停下来，而是会去执行我们提前在服务任务中配置好的方法。这里有三种不同的方式来设置这里的任务，分别是监听类、委托表达式、表达式。

服务任务还可以设置执行监听器

###### 3、流程模型文件

```
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:flowable="http://flowable.org/bpmn" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema" targetNamespace="http://flowable.org/test" exporter="Flowable Open Source Modeler" exporterVersion="6.7.2.32">
  <process id="ces-12-06" name="请假申请" isExecutable="true">
    <documentation>请假申请</documentation>
    <startEvent id="startEvent1" name="请假" flowable:formKey="ces01-04" />
    <userTask xmlns:custom="http://flowable.org/modeler" id="Activity_1ykd3r9" name="经理审批" flowable:formKey="ces010402" flowable:assignee="admin" flowable:candidateUsers="admin" custom:userinfo="" custom:orgid="1535984304478359552" custom:approverstype="1">
      <extensionElements>
        <flowable:taskListener class="me.flyray.bsin.server.listener.UserTaskListener" event="create" />
        <custom:initiator-can-complete>false</custom:initiator-can-complete>
      </extensionElements>
    </userTask>
    <serviceTask id="Activity_0p3bnbk" name="系统审批" flowable:class="me.flyray.bsin.server.impl.ServiceTask" />
    <endEvent id="Event_0epgk6f" name="请假完成" />
    <sequenceFlow id="Flow_1jj8ggp" sourceRef="Activity_0p3bnbk" targetRef="Event_0epgk6f" />
    <sequenceFlow id="Flow_1lmtyxz" sourceRef="Activity_1ykd3r9" targetRef="Event_0epgk6f" />
    <sequenceFlow id="Flow_1s303z4" name="请假天数大于三天" sourceRef="Gateway_10u5rdb" targetRef="Activity_1ykd3r9">
      <conditionExpression xsi:type="tFormalExpression">${days&gt;10}</conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="Flow_0mld934" name="请假天数小于等于三天" sourceRef="Gateway_10u5rdb" targetRef="Activity_0p3bnbk">
      <conditionExpression xsi:type="tFormalExpression">${days&lt;10}</conditionExpression>
    </sequenceFlow>
    <exclusiveGateway id="Gateway_10u5rdb" name="互斥网关" />
    <sequenceFlow id="Flow_01z8dgk" sourceRef="startEvent1" targetRef="Gateway_10u5rdb" />
  </process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_ces-12-06">
    <bpmndi:BPMNPlane id="BPMNPlane_ces-12-06" bpmnElement="ces-12-06">
      <bpmndi:BPMNEdge id="BPMNEdge_Flow_01z8dgk" bpmnElement="Flow_01z8dgk" flowable:sourceDockerX="20.0" flowable:sourceDockerY="20.0" flowable:targetDockerX="20.0" flowable:targetDockerY="20.0">
        <omgdi:waypoint x="-300.05000243812856" y="130" />
        <omgdi:waypoint x="-240" y="130" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="BPMNEdge_Flow_0mld934" bpmnElement="Flow_0mld934" flowable:sourceDockerX="20.0" flowable:sourceDockerY="20.0" flowable:targetDockerX="45.0" flowable:targetDockerY="35.0">
        <omgdi:waypoint x="-200.05415451895044" y="130" />
        <omgdi:waypoint x="-25.00000000005869" y="130" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="BPMNEdge_Flow_1s303z4" bpmnElement="Flow_1s303z4" flowable:sourceDockerX="20.0" flowable:sourceDockerY="20.0" flowable:targetDockerX="45.0" flowable:targetDockerY="35.0">
        <omgdi:waypoint x="-220" y="110" />
        <omgdi:waypoint x="-220" y="10" />
        <omgdi:waypoint x="-25.000000000000085" y="10" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="BPMNEdge_Flow_1lmtyxz" bpmnElement="Flow_1lmtyxz" flowable:sourceDockerX="45.0" flowable:sourceDockerY="35.0" flowable:targetDockerX="20.0" flowable:targetDockerY="20.0">
        <omgdi:waypoint x="64.94999999999695" y="10" />
        <omgdi:waypoint x="190" y="10" />
        <omgdi:waypoint x="190" y="110" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="BPMNEdge_Flow_1jj8ggp" bpmnElement="Flow_1jj8ggp" flowable:sourceDockerX="45.0" flowable:sourceDockerY="35.0" flowable:targetDockerX="20.0" flowable:targetDockerY="20.0">
        <omgdi:waypoint x="64.94999999999342" y="130" />
        <omgdi:waypoint x="170" y="130" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="BPMNShape_startEvent1" bpmnElement="startEvent1">
        <omgdc:Bounds x="-340" y="110" width="40" height="40" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="BPMNShape_Activity_1ykd3r9" bpmnElement="Activity_1ykd3r9">
        <omgdc:Bounds x="-25" y="-25" width="90" height="70" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="BPMNShape_Activity_0p3bnbk" bpmnElement="Activity_0p3bnbk">
        <omgdc:Bounds x="-25" y="95" width="90" height="70" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="BPMNShape_Event_0epgk6f" bpmnElement="Event_0epgk6f">
        <omgdc:Bounds x="170" y="110" width="40" height="40" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="BPMNShape_Gateway_10u5rdb" bpmnElement="Gateway_10u5rdb" isMarkerVisible="true">
        <omgdc:Bounds x="-240" y="110" width="40" height="40" />
      </bpmndi:BPMNShape>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</definitions>
```



##### 		4、模型管理

![avatar](../docs/image/模型管理.png)

##### 		5、模型类型

![avatar](../docs/image/模型类型.png)

##### 		6、流程实例

![avatar](../docs/image/流程实例.png)

##### 		7、流程任务

##### 		8、历史流程实例

​		

#### 五、注意事项
