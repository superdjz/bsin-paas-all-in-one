package me.flyray.bsin.server.impl;

import me.flyray.bsin.constants.ResponseCode;
import me.flyray.bsin.exception.BusinessException;
import me.flyray.bsin.facade.response.ModelTypeTree;
import me.flyray.bsin.facade.service.BsinAdminModelTypeService;
import me.flyray.bsin.server.biz.ModelTypeBiz;
import me.flyray.bsin.server.domain.ModelType;
import me.flyray.bsin.server.mapper.ModelTypeMapper;
import me.flyray.bsin.server.util.BsinServiceContext;
import me.flyray.bsin.utils.RespBodyHandler;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 模型类型
 */

public class ModelTypeServiceImpl implements BsinAdminModelTypeService {
    @Autowired
    private ModelTypeMapper modelTypeMapper;
    @Autowired
    private ModelTypeBiz modelTypeBiz;

    /**
     * 查看所有模型类型
     * @param requestMap
     * @return
     */
    @Override
    public Map<String, Object> getModelTypeTree(Map<String, Object> requestMap) {
        String tenantId = (String) requestMap.get("tenantId");
        List<ModelType> modelTypes = modelTypeMapper.getModelTypeListByTenantId(tenantId);
        // 组装成父子的树形目录结构
        // 查询所有的一级机构(parentId=-1)
        List<ModelTypeTree> treeList = modelTypes.stream().filter(modelType -> modelType.getParentId().equals("-1"))
                .map(m -> {
                    ModelTypeTree levelModelType = new ModelTypeTree(m.getId(), m.getTypeCode(), m.getTypeName(),
                            m.getParentId(), m.getDescription(),m.getDelFlag(),m.getCreateTime(),
                            m.getUpdateTime(), modelTypeBiz.getModelTypeTree(m, modelTypes));
                    return levelModelType;
                }).collect(Collectors.toList());
        return RespBodyHandler.setRespBodyListDto(treeList);
    }

    /**
     * ID,名称模糊，编号查看模型类型
     * @param requestMap
     * @return
     */
    @Override
    public Map<String, Object> getModelTypeList(Map<String, Object> requestMap) {
        ModelType modelType = BsinServiceContext.getReqBodyDto(ModelType.class, requestMap);
        List<ModelType> typeByNameAndCode = modelTypeMapper.selectModelTypeList(modelType);
        return RespBodyHandler.setRespBodyListDto(typeByNameAndCode);
    }

    /**
     *生成新的模型类型
     * @param requestMap
     * @return
     */
    @Override
    public Map<String, Object> add(Map<String, Object> requestMap) {
        ModelType modelType = BsinServiceContext.getReqBodyDto(ModelType.class, requestMap);
        try{
            modelTypeMapper.insertModelType(modelType);
        }catch (Exception e){
            System.out.println(e.getMessage());
            throw new BusinessException(ResponseCode.MODEL_TYPE_ADD_FAIL);
        }
        return RespBodyHandler.RespBodyDto();
    }

    /**
     * 修改模型类型
     * @param requestMap
     * @return
     */
    @Override
    public Map<String, Object> edit(Map<String, Object> requestMap) {
        ModelType modelType = BsinServiceContext.getReqBodyDto(ModelType.class, requestMap);
        try{
            modelTypeMapper.updateModelTypeById(modelType);
        }catch (Exception e){
            System.out.println(e.getMessage());
            throw new BusinessException(ResponseCode.MODEL_TYPE_UPDATE_FAIL);
        }

        return RespBodyHandler.RespBodyDto();
    }

    /**
     * 删除模型类型
     * @param requestMap
     * @return
     */
    @Override
    public Map<String, Object> delete(Map<String, Object> requestMap) {
        String id=(String)requestMap.get("id");
        modelTypeMapper.deleteById(id);
        return RespBodyHandler.RespBodyDto();
    }
}
