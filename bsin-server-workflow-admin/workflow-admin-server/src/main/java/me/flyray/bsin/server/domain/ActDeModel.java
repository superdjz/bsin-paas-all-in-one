package me.flyray.bsin.server.domain;


import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActDeModel {
    private String id;
    private String name;
    private String key;
    private String description;
    private String modelComment;
    private Date created;
    private String createdBy;
    private Date lastUpdated;
    private String lastUpdatedBy;
    private Integer version;
    private String modelEditorJson;
    private String thumbnail;
    private Integer modelType;
    private String tenantId;
}
