# s11eDao-ipfs-oss
s11eDao IPFS OSS 是s11e-dao推出的基于IPFS的对象存储服务(OSS)，除了服务于s11eDao的[bigan]()和[daobook]()产品中的去中心化存储外（主要为元数据的存储管理），也对外提供免费的去中心化OSS API接口服务和OSS管理后台。

## IPFS概述
- IPFS 是一个全局的、版本化的、点对点的文件系统。它结合了以前系统（如 Git、BitTorrent、Kademlia、SFS 和 Web）的好主意。它就像一个单一的 BitTorrent 群，交换 git 对象。IPFS 提供了一个与 HTTP 网络一样简单的接口，但内置了永久性。你也可以在 /ipfs 挂载世界。
- 星际文件系统(InterPlanetary File System). IPFS 是一个分布式的web, 点到点超媒体协议. 可以让我们的互联网速度更快, 更加安全, 并且更加开放. IPFS协议的目标是取代传统的互联网协议HTTP。

- 为什么有IPFS
>* 众所周知, 互联网是建立在HTTP协议上的. HTTP协议是个伟大的发明, 让我们的互联网得以快速发展.但是互联网发展到了今天HTTP逐渐出来了不足.
>* HTTP的中心化是低效的, 并且成本很高
>* 使用HTTP协议每次需要从中心化的服务器下载完整的文件(网页, 视频, 图片等), 速度慢, 效率低. 如果改用P2P的方式下载, 可以节省近60%的带宽. P2P将文件分割为小的块, 从多个服务器同时下载, 速度非常快.
>* Web文件经常被删除
>* 回想一下是不是经常你收藏的某个页面, 在使用的时候浏览器返回404(无法找到页面), http的页面平均生存周期大约只有100天. Web文件经常被删除(由于存储成本太高), 无法永久保存. IPFS提供了文件的历史版本回溯功能(就像git版本控制工具一样), 可以很容易的查看文件的历史版本, 数据可以得到永久保存
>* 中心化限制了web的成长
>* 我们的现有互联网是一个高度中心化的网络. 互联网是人类的伟大发明, 也是科技创新的加速器. 各种管制将对这互联网的功能造成威胁, 例如: 互联网封锁, 管制, 监控等等. 这些都源于互联网的中心化.而分布式的IPFS可以克服这些web的缺点.
>* 互联网应用高度依赖主干网
>* 主干网受制于诸多因素的影响, 战争, 自然灾害, 互联网管制, 中心化服务器宕机等等, 都可能是我们的互联网应用中断服务. IPFS可以是互联网应用极大的降低互联网应用对主干网的依赖.

- IPFS的目标
>* IPFS不仅仅是为了加速web. 而是为了最终取代HTTP协议, 使互联网更加美好

## S11eDao IPFS OSS服务介绍
### bigan和dapbook链上资产metadata存储管理
- 存储文件结构约定
>* ipfs 采用 merkle dag 的方式来组织文件，这和类 unix 系统中的文件系统非常类似，S11eDao IPFS 的文件结构如下所示：
~~~bash
.                               # IPFS 根目录
├── bigan                       # 所有通过bigan发行铸造的链上资产的IPFS存储文件放在此目录下
│   └── 1-1234                  # 为每一个租户生成一个文件夹，文件夹名称为：自增ID-Keccak-256(租户ID)的后4位
│       ├── 1155-name           # 为每一个租户创建的collection创建一个文件夹，文件夹名称为：集合类型(1155/721/3525)-集合的name，所有该集合下的元数据存放于此目录下
│       │   └── 1.png           # 元数据图片文件名称为>1 & < supply的整型，元数据图片需要在部署合约之后上传
│       │   └── 1.json|         # 元数据json文件名称为>1 & < supply的整型，元数据(json文件)在铸造NFT的时候生成并上传至此目录下
│       └── 721-name
│   └── 2
│       └── 3525-name
└── daobook                      # 所有daobook中的涉及IPFS存储的文件放在此目录下
|   └── s11e-dao                 # s11e-dao中的ipfs存储内容目录
|      └── daos-profile          # dao-profile中的元数据
│          └── 1.png             # 元数据图片文件名称为>1 & < supply的整型，元数据图片需要在部署合约之后上传
│          └── 1.json|           # 元数据json文件名称为>1 & < supply的整型，元数据(json文件)在铸造NFT的时候生成并上传至此目录下
|   ├── 1-1234                   # 通过s11e-dao创建的dao的元数据存储目录--文件名称为：自增ID-Keccak-256(dao name)的后4位
|   │   └── member-pfp
|   │   └── nft-collection
~~~

- bigan元数据管理
>* 文件夹创建
>>* 当租户注册bigan开发平台后，便为租户在"/bigan/"目录下创建 "自增ID-Keccak-256(租户ID)的后4位" 的文件夹；  

>* 元数据上传管理
>>* 租户登录bigan后台之后，通过元数据管理菜单栏，便可维护管理自己的元数据文件，每一个合约对应一个文件夹。eg：租户想要部署了一个name为“Boring ape”的ERC721的智能合约，则可以创建一个 "721-boring-ape"的文件夹(此文件夹名称不做限制，满足文件夹命名规范即可)
>>* 创建好文件夹后，即可将元数据图片依次添加至此文件夹中，图片名称会在上传过程中强制改为 >1 & < supply 的整型数字
>>* metadata的json文件会在nft铸造的时候根据元数据模板自动生成，并命名为 “tokenId.json”，上传至当前文件夹下


- daobokk元数据管理
>* 文件夹创建
>>* 在部署s11e-dao-protocol合约的时候会在 "/daobook/“ 目录下创建 ”s11e-dao" 文件夹，用于管理存储所有s11e-dao的元数据  
>>* 通过daobook创建一个新的dao时，并在 "/daobook/“ 目录下创建 “自增ID-Keccak-256(dao name)的后4位” 的文件夹，用于春存储管理 "dao-name" 的所有元数据  

>* 元数据上传管理
>>* 当dao创建国库资产或者涉及到IPFS存储的时候，并在此 dao 的所属目录下创建相应的文件夹，管理元数据


### S11eDao IPFS OSS的 IPNS
- IPNS全称是Inter-Planetary Naming Service，星际命名系统。这个系统可以把我们现在访问互联网使用的URL地址映射成IPFS系统中的一串哈希值。
- 星际名称系统(IPNS)是一个创建个更新可变的链接到IPFS内容的系统，由于对象在IPFS中是内容寻址的，他们的内容变化将导致地址随之变化。对于多变的事物是有用的。但是很难获取某些内容的最新版本。
- 在IPNS中名字是被哈希的公钥。它与一条记录相关联，该记录包含有关其链接的哈希的信息，该信息由相应的私钥签名。新的记录可以在任何时候被签名与发布。

#### IPNS问题点
- 有效时间

- 发布时间

#### S11eDao IPFS OSS的 IPNS管理

- S11eDao中NFT中的tokenURI= baseURI + "/collection name/” + tokenId.json 格式；
- 任何文件夹中的文件变化，都会导致文件夹的CID的改变，因此为了保证 tokenURI 的固定，需要保证在更改(追加、删除、修改)过 collection 文件夹中的文件时，都能通过 baseURI 找到 metadata，需要使用iPFS提供的IPNS功能
- S11eDao IPFS OSS 为每一个通过bigan注册的租户或者通过daobook创建的dao组织创建一个 IPNS，这样更改过租户或者dao组织下的文件的时候，重新publish IPNS，就可以保证通过 baseURI 都能访问到 metadata. 



### S11eDao IPFS OSS的 DNSLink
- DNS链接使用DNS TXT记录映射域名(如ipfs.io)到一个IPFS地址。因为你可以编辑自己的DNS记录，可以使他们总是指向最新版本的IPFS中的对象(如果修改了IPFS中的对象则IPFS中的对象地址也会改变)。由于DNS链接使用DNS记录，所以可以设计名字/路径/(子)域/任何容易分类，阅读和记的名字。
- 一个DNS链接地址看起来像一个IPNS地址，但是DNS链接使用域名代替了被哈希的公钥:
~~~
/ipns/proofs.filecoin.io
~~~

- 如何使用DNSLink
>* 当一个IPFS客户端或者节点尝试解析一个地址，将会寻找前缀为dnslink=的TXT记录。剩下的可以是/ipfs/链接或者是/ipns/，或者是链接到其他的DNSLink。
~~~bash
dnslink=/ipfs/<具体内容的CID>
# 例如，_dnslink.docs.ipfs.io的DNS记录继续了解DNS链接实体：
$ dig +noall +answer TXT _dnslink.docs.ipfs.io
_dnslink.docs.ipfs.io.  34  IN  TXT "dnslink=/ipfs/QmVMxjouRQCA2QykL5Rc77DvjfaX6m8NL6RyHXRTaZ9iya"

# 基于这个地址：/ipns/docs.ipfs.io/introduction/
# 可以获取这个区块：/ipfs/QmVMxjouRQCA2QykL5Rc77DvjfaX6m8NL6RyHXRTaZ9iya/introduction/
~~~
