package me.flyray.bsin.facade.service;


import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
* @author bolei
* @description 针对表【ai_tenant_ai_model】的数据库操作Service
* @createDate 2023-04-25 18:40:35
*/

@Path("tenantAiModelService")
public interface AiModelService {

    /**
     *添加
     */
    @POST
    @Path("add")
    @Produces("application/json")
    Map<String,Object> add(Map<String,Object> requestMap);

    /**
     *删除
     */
    @POST
    @Path("delete")
    @Produces("application/json")
    Map<String,Object> delete(Map<String,Object> requestMap);

    /**
     *编辑
     */
    @POST
    @Path("edit")
    @Produces("application/json")
    Map<String,Object> edit(Map<String,Object> requestMap);

    /**
     *详情
     */
    @POST
    @Path("detail")
    @Produces("application/json")
    Map<String,Object> detail(Map<String,Object> requestMap);

    /**
     *分页查询
     */
    @POST
    @Path("getPageList")
    @Produces("application/json")
    Map<String,Object> getPageList(Map<String,Object> requestMap);

}
